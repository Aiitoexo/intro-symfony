allTweet = document.querySelector('.all_tweet');

let flag = true;

allTweet.addEventListener('click', async function (event) {

    const likeLink = event.target.closest('.link-like')
    if (!likeLink) return;

    console.log(event.target.closest('.link-like'))

    event.preventDefault();

    const url = likeLink.href;

    const img = likeLink.querySelector('img')
    const parent = likeLink.closest('.card-footer')
    const span = parent.querySelector('span')


    if (flag === true) {

        flag = false
        const response = await fetch(url)

        if (response) {

            if (likeLink.classList.contains('link-liked')) {
                img.src = img.src.replace('fill', 'empty')
                span.textContent = parseInt(span.textContent) - 1;
                likeLink.classList.remove('link-liked')
            } else {
                span.textContent = parseInt(span.textContent) + 1;
                img.src = img.src.replace('empty', 'fill')
                likeLink.classList.add('link-liked')
            }
            console.log(response)

            flag = true;

        }

    }

})