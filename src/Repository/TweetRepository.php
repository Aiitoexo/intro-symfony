<?php

namespace App\Repository;

use App\Entity\Tweet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tweet|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tweet|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tweet[]    findAll()
 * @method Tweet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TweetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tweet::class);
    }

    public function allTweetUser($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.user_id = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function allTweetFriends()
    {
        return $this->createQueryBuilder('t')
            ->where('t.parent is NULL')
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }


    public function allTweetProfileFriend($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.user_id = :val')
            ->setParameter('val', $value)
            ->orderBy('t.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function tweet($value)
    {
        return $this->find($value);

    }

    public function comment($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.parent = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult();
    }



//
//    public function findAuthorByTweet($value) {
//        return $this->createQueryBuilder('tweet')
//            ->
//    }

// /**
//  * @return Tweet[] Returns an array of Tweet objects
//  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tweet
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
