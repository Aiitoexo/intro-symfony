<?php


namespace App\Controller;

use App\Entity\Tweet;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="app_profilePage")
     * @param Request $req
     * @return Request
     */
    public function profilePage(Request $req)
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $newTweet = new Tweet();

        $newTweet->setUserId($user, $user->getId());

        $form = $this->createFormBuilder($newTweet)
            ->add('message', TextAreaType::class)
            ->getForm();

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $newTweet->setCreatedAt(new DateTime());

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($newTweet);
            $manager->flush();

            return $this->redirectToRoute('app_profilePage');
        }

        $tweets = $this->getDoctrine()
            ->getRepository(Tweet::class)
            ->allTweetUser($user);

        return $this->render('page/profile.html.twig', [
            'title' => 'Profile | Linked-Infrep',
            'tweet' => $tweets,
            'formNews' => $form->createView(),
            'user' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'picture' => $user->getPicture(),
            'years' => $user->getYears()
        ]);
    }
}