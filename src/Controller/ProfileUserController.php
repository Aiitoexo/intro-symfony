<?php


namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProfileUserController extends AbstractController
{
    /**
     * @Route("/profile-user/{id}", name="app_profileUserPage")
     */
    public function index(Request $req, int $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $user_profile = $this->getDoctrine()
            ->getRepository(User::class)
            ->user_profil($id);

        $tweets = $this->getDoctrine()
            ->getRepository(Tweet::class)
            ->allTweetProfileFriend($id);

        dump($user_profile);

        return $this->render('page/profilte_user.html.twig', [
            'title' => 'Linked-Infrep | ' . $user_profile->getFirstName() . " " . $user_profile->getLastName(),
            'tweet' => $tweets,
            'user' => $user->getFirstName(),
            'first_name' => $user_profile->getFirstName(),
            'last_name' => $user_profile->getLastName(),
            'picture' => $user_profile->getPicture(),
            'years' => $user_profile->getYears()
        ]);
    }
}