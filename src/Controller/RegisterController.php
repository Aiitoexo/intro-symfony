<?php


namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use function pathinfo;
use function uniqid;
use const PATHINFO_FILENAME;

class RegisterController extends AbstractController
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/register", name="app_registerPage")
     * @param Request $req
     * @return Request
     */
    public function registerPage(Request $req)
    {
        $newUser = new User();

        $form = $this->createFormBuilder($newUser)
            ->setMethod('POST')
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('first_name', TextType::class)
            ->add('last_name', TextType::class)
            ->add('years', TextType::class)
            ->add('picture', FileType::class, [
                'mapped' => false
            ])
            ->getForm();

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $password = $newUser->getPassword();

            $picture = $form->get('picture')->getData();

            if ($picture) {
                $originalFilename = pathinfo($picture->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $originalFilename;
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $picture->guessExtension();

                try {
                    $picture->move(
                        $this->getParameter('profile_picture'),
                        $newFilename
                    );
                    $newUser->setPicture($newFilename);
                } catch (FileException $e) {

                }

            }

            $newUser->setPassword($this->passwordEncoder->encodePassword(
                $newUser,
                $password
            ));

            $manager->persist($newUser);
            $manager->flush();

            return $this->redirectToRoute('app_profilePage');
        }

        return $this->render('page/register.html.twig', [
            'title' => 'Register',
            'formRegister' => $form->createView(),
        ]);
    }
}