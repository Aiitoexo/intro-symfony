<?php


namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FriendController extends AbstractController
{
    /**
     * @Route("/friends", name="app_friendsPage")
     */
    public function friendsPage(Request $req)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $newTweet = new Tweet();

        $newTweet->setUserId($user, $user->getId());

        $form = $this->createFormBuilder($newTweet)
            ->add('message', TextAreaType::class)
            ->getForm();

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $newTweet->setCreatedAt(new DateTime());
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($newTweet);
            $manager->flush();

            return $this->redirectToRoute('app_feedPage');
        }


        $all_users = $this->getDoctrine()
            ->getRepository(User::class)
            ->all_users();

        return $this->render('page/friends.html.twig', [
            'title' => 'Friends | Linked-Infrep',
            'user' => $user->getFirstName(),
            'all_users' => $all_users,
        ]);
    }
}