<?php


namespace App\Controller;


use App\Entity\Tweet;
use App\Entity\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class TweetController extends AbstractController
{
    /**
     * @Route("/tweet/{id}", name="app_tweetPage")
     */
    public function tweet(Request $req, int $id)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $newTweet = new Tweet();

        $newTweet->setUserId($user, $user->getId());

        $tweet = $this->getDoctrine()
            ->getRepository(Tweet::class)
            ->tweet($id);

        $comment = $this->getDoctrine()
            ->getRepository(Tweet::class)
            ->comment($id);

        $form = $this->createFormBuilder($newTweet)
            ->add('message', TextAreaType::class)
            ->getForm();

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $newTweet->setCreatedAt(new DateTime());
            $newTweet->setParent($tweet, $tweet->getParent());
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($newTweet);
            $manager->flush();

            return $this->redirectToRoute('app_tweetPage', ['id' => $newTweet->getId()]);
        }

        return $this->render('page/tweet.html.twig', [
            'title' => 'Linked-Infrep | ',
            'tweet' => $tweet,
            'comments' => $comment,
            'formNews' => $form->createView(),
            'user' => $user->getFirstName(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'picture' => $user->getPicture(),
            'years' => $user->getYears()
        ]);
    }

    /**
     * @Route("/tweet/{id}/like", name="app_tweet_like")
     * @param int $id
     * @return Response
     */
    public function toggleLike(int $id, Request $request): Response {

        // On récupère l'utilisateur connecté
        /**
         * @var User $user
         */
        $user = $this->getUser();

        // On récupère le tweet demandé
        $tweet = $this->getDoctrine()->getRepository(Tweet::class)->find($id);

        // ON VÉRIFIE QUE CE TWEET EXISTE !!!
        if (empty($tweet)) {
            throw new NotFoundHttpException("Le tweet d'id : $id n'existe pas.");
        }

        // On modifie la relation
        if ($user->doesLike($tweet)) {
            $user->unlikeTweet($tweet);
        } else {
            $user->likeTweet($tweet);
        }

        // On enregistre en BDD
        $this->getDoctrine()->getManager()->flush();


        // On retourne la réponse du serveur

        // On essaye de rediriger sur la route d'ou l'on vient
        $referer = $request->headers->get('referer');
        if (!empty($referer)) { // le referer n'est peut être pas envoyé par le client !!
            $url = $referer . "#tweet-" .$id; // on ajoute l'ancre pour régler le niveau du scroll
            return $this->redirect($url);
        }

        // sinon on redirige vers la page single
        return $this->redirectToRoute("app_tweet_single", ['id' => $id]);

    }
}