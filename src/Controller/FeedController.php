<?php


namespace App\Controller;

use App\Entity\Tweet;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FeedController extends AbstractController
{
    /**
     * @Route("/feed", name="app_feedPage")
     */
    public function feedPage(Request $req)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $newTweet = new Tweet();

        $newTweet->setUserId($user, $user->getId());

        $form = $this->createFormBuilder($newTweet)
            ->add('message', TextAreaType::class)
            ->getForm();

        $form->handleRequest($req);

        if ($form->isSubmitted() && $form->isValid()) {
            $newTweet->setCreatedAt(new DateTime());
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($newTweet);
            $manager->flush();

            return $this->redirectToRoute('app_feedPage');
        }

        $user = $this->getUser();

        $tweets = $this->getDoctrine()
            ->getRepository(Tweet::class)
            ->allTweetFriends();

        return $this->render('page/feed.html.twig', [
            'title' => 'Linkd-Infrep',
            'user' => $user->getFirstName(),
            'tweet' => $tweets,
            'formNews' => $form->createView(),
        ]);
    }
}